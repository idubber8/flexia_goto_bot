from flask import Flask
from flask_sslify import SSLify
from flask import request
from flask import jsonify
import requests
import json
import telebot
from models import *

app = Flask(__name__)
sslify = SSLify(app)
bot = telebot.TeleBot(constants.token)

model = KeyedVectors.load_word2vec_format(constants.model_name, binary=False)
tree = build_tree(gen_vec_table(model))

URL = 'https://api.telegram.org/bot483545294:AAELyKZast8NSs_fITyd5tszWaVzoahVqCk/'

def write_json(data, filename='answer.json'):
    with open(filename, 'w') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)

def send_message(chat_id, text='blablabla'):
    url = URL + 'sendMessage'
    answer = {'chat_id': chat_id, 'text' : text}
    r = requests.post (url, json=answer)
    return r.json()

@app.route("/setWebhook")
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url="https://felixkad.pythonanywhere.com/483545294:AAELyKZast8NSs_fITyd5tszWaVzoahVqCk/")
    return "!", 200

@app.route('/483545294:AAELyKZast8NSs_fITyd5tszWaVzoahVqCk/',methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        r = request.get_json()
        chat_id = r['message']['chat']['id']
        message = r['message']['text']
        vec = gen_quest_vec(message, model)
        if vec == []:
            send_message(chat_id, text = "Я не могу ответить на этот вопрос, если он выжный, то вы можете позвонить и уточнить этот вопрос по телефону +7 985 244 32 26")
        else:
            answer = get_answer(constants.quest_ans, find_similar(tree, vec))
            send_message(chat_id, text = answer)
        return jsonify(r)
    return '<h1>BOT running<h1>'

if __name__ == '__main__':
    app.run()