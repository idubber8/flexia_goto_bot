import constants
from datetime import datetime
import time
import gensim.models as g
import csv
import gensim
from gensim.models import KeyedVectors
from pymystem3 import Mystem
import numpy as np
from sklearn.decomposition import PCA
from typing import List
from scipy import spatial
import codecs
all = 84
num = 0
MyStem = Mystem()

# an embedding word with associated vector
class Word:
    def __init__(self, text, vector):
        self.text = text
        self.vector = vector


# a sentence, a list of words
class Sentence:
    def __init__(self, word_list):
        self.word_list = word_list

    # return the length of a sentence
    def len(self) -> int:
        return len(self.word_list)


# todo: get the frequency for a word in a document set
def get_word_frequency(word_text):
    return 1.0


# A SIMPLE BUT TOUGH TO BEAT BASELINE FOR SENTENCE EMBEDDINGS
# Sanjeev Arora, Yingyu Liang, Tengyu Ma
# Princeton University
# convert a list of sentence with word2vec items into a set of sentence vectors
def sentence_to_vec(sentence_list: List[Sentence], embedding_size: int, a: float=1e-5):
    sentence_set = []
    for sentence in sentence_list:
        vs = np.zeros(embedding_size)  # add all word2vec values into one vector for the sentence
        sentence_length = sentence.len()
        for word in sentence.word_list:
            a_value = a / (a + get_word_frequency(word.text))  # smooth inverse frequency, SIF
            vs = np.add(vs, np.multiply(a_value, word.vector))  # vs += sif * word_vector

        vs = np.divide(vs, sentence_length)  # weighted average
        sentence_set.append(vs)  # add to our existing re-calculated set of sentences
    # calculate PCA of this sentence set
    pca = PCA(n_components=embedding_size)
    pca.fit(np.array(sentence_set))
    u = pca.components_[0]  # the PCA vector
    u = np.multiply(u, np.transpose(u))  # u x uT

    # pad the vector?  (occurs if we have less sentences than embeddings_size)
    if len(u) < embedding_size:
        for i in range(embedding_size - len(u)):
            u = np.append(u, 0)  # add needed extension for multiplication below

    # resulting sentence vectors, vs = vs -u x uT x vs
    sentence_vecs = []
    for vs in sentence_set:
        sub = np.multiply(u,vs)
        sentence_vecs.append(np.subtract(vs, sub))
    return sentence_vecs

def get_sent(list_of_words, model):
    Sent = []
    for i in list_of_words:
        if i in model:
            Sent.append(Word(i,model[i]))
    return Sent

def get_lemword_with_type(text, MyStem=MyStem, zamena_dict={"A":"ADJ","ADV":"ADV","ADVPRO":"ADV", "APRO":"ADJ","CONJ":"CCONJ","INTJ":"INTJ","NUM":"NUM","PART":"PART","PR":"ADP","S":"NOUN","SPRO":"PRON","V":"VERB"}):
    global num
    if text == "/start" or text == "/help":
        return ["старт_NOUN"]
    words_with_type = []
    text = get_norm_text(text)
    lemtext = MyStem.lemmatize(text)
    if '\n' in lemtext:
        lemtext.remove('\n')
    types = []
    num+=1
    for i in MyStem.analyze("".join(lemtext)):
        if i == {'text': ' '} or i == {'text': '\n'} or i == {'text': ' \n'} or i == {'text': '  '} :
            pass
        else:
            try:
                a = i['analysis'][0]['gr']
            except:
                continue
            b = a.find("=")
            types.append(a[:b].split(",")[0])
    words = ("".join(lemtext)).split()
    for i in range(len(types)):
        words_with_type.append(words[i]+"_"+ zamena_dict[types[i]])
    return words_with_type
def gen_quest_vec(quest ,model):
    sent_block = sentence_to_vec([Sentence([Word(word, model[word]) for word in get_true_words(get_lemword_with_type(quest),model)]), Sentence([Word(word, model[word]) for word in get_true_words(get_lemword_with_type("Привет, как дела?"),model)])], embedding_size=300)
    return sent_block[0]

def gen_Sent_cl(text, model):
    sent = get_sent(get_lemword_with_type(text),model)
    return Sentence(sent)
def get_norm_text(text):
    norm_text = text.lower()
    norm_text = norm_text.replace('<br />', ' ')
    STOSYMBOL = ['.', '"', ',', '(', ')', '!', '?', ';', ':', '/', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    for char in STOSYMBOL:
        norm_text = norm_text.replace(char, '')
    return norm_text

def normalize_text(text):
    norm_text = text.lower()
    norm_text = norm_text.replace('<br />', ' ')
    for char in ['.', '"', ',', '(', ')', '!', '?', ';', ':', '/']:
        norm_text = norm_text.replace(char, ' ' + char + ' ')
    return norm_text

def get_true_words(word_list, model):
    result = []
    for word in word_list:
        if word in model:
            result.append(word)

    if result == []:
        return ['морепродукт_NOUN', 'ананас_NOUN']
    return result

def gen_vec_table(model, file_name = constants.quest_ans):
    with codecs.open(file_name, 'r', 'cp1251') as fp:
        reader = csv.reader(fp, delimiter=';')
        data_read = [row for row in reader]
    sent_n = [(Sentence([Word(word, model[word]) for word in get_true_words(get_lemword_with_type(quest[0]),model)])) for quest in data_read]
    return sentence_to_vec(sent_n, constants.vector_size)

def build_tree(vec_table):
    return spatial.KDTree(vec_table)

def find_similar(tree, vec):
    dist, numb = list(map(list, tree.query(vec, 3)))
    print(dist, numb)
    return int(numb[0])

def get_answer(file_name, numb):
    with codecs.open(file_name, 'r', 'cp1251') as fp:
        reader = csv.reader(fp, delimiter=';')
        data_read = [row for row in reader]
    return data_read[numb][1]

def log(message, answer):
    print("\n" + str(datetime.now()))
    print("Сообщение от {0} {1}. (id = {2}) \nТекст: {3}".format(message.from_user.first_name, message.from_user.last_name, str(message.from_user.id), message.text))
    print("Ответ:", answer)
